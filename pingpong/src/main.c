#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

#include "ball.h"
#include "buttons.h"
#include "buzzer.h"

#define LEFT_BUTTON_PIN PC1
#define RIGHT_BUTTON_PIN PC3
#define BALL_PIN PC2
#define BUZZER_PIN PD3

volatile int soundPlaying = 0;  // Flag to indicate if the sound is playing

void initButtons()
{
    DDRC &= ~((1 << LEFT_BUTTON_PIN) | (1 << RIGHT_BUTTON_PIN));  // Set button pins as input
    PORTC |= (1 << LEFT_BUTTON_PIN) | (1 << RIGHT_BUTTON_PIN);     // Enable pull-up resistors for the buttons
}

int isLeftButtonPressed()
{
    return (PINC & (1 << LEFT_BUTTON_PIN)) == 0;
}

int isRightButtonPressed()
{
    return (PINC & (1 << RIGHT_BUTTON_PIN)) == 0;
}

void initBall()
{
    DDRC |= (1 << BALL_PIN);   // Set ball pin as output
}

void moveBall(int direction)
{
    if (direction == 0)
        PORTC &= ~(1 << BALL_PIN);  // Move the ball left
    else
        PORTC |= (1 << BALL_PIN);   // Move the ball right
}

void initUSART()
{
    UBRR0H = 0;
    UBRR0L = 103;                              // Set baud rate to 9600 for 8MHz clock frequency
    UCSR0B = (1 << TXEN0);                      // Enable transmitter
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);     // Set data frame: 8-bit data, 1 stop bit
}

void transmitByte(uint8_t data)
{
    while (!(UCSR0A & (1 << UDRE0)))
        ;
    UDR0 = data;
}

void transmitString(const char* string)
{
    while (*string)
    {
        transmitByte(*string);
        string++;
    }
}

void printBinaryByte(uint8_t byte)
{
    for (int8_t bit = 7; bit >= 0; bit--)
    {
        if (byte & (1 << bit))
            transmitByte('1');
        else
            transmitByte('0');
    }
}

void enableBuzzer()
{
    DDRD |= (1 << BUZZER_PIN);    // Set buzzer pin as output
}

void disableBuzzer()
{
    DDRD &= ~(1 << BUZZER_PIN);    // Set buzzer pin as input
    PORTD &= ~(1 << BUZZER_PIN);   // Turn off the buzzer
}

void playTone(float frequency, uint32_t duration)
{
    soundPlaying = 1;  // Set the soundPlaying flag

    uint32_t periodInMicro = (uint32_t)(1000000 / frequency);  // Calculate the period in microseconds from the frequency
    uint32_t durationInMicro = duration * 1000;               // We express duration in microseconds
    uint32_t endTime = micros() + durationInMicro;            // Calculate the end time

    while (micros() < endTime)  // Play the tone until the end time
    {
        if (isLeftButtonPressed() || isRightButtonPressed())
        {
            soundPlaying = 0;  // Clear the soundPlaying flag
            return;            // Exit the function if a button is pressed
        }

        PORTD ^= (1 << BUZZER_PIN);              // Toggle the buzzer pin
        _delay_us(periodInMicro / 2);            // Wait for half of the period
    }

    disableBuzzer();  // Turn off the buzzer after playing the tone
}

int main()
{
    initUSART();
    initButtons();
    initBall();

    int ballDirection = 0;  // 0 for left, 1 for right
    int ballPosition = 32;  // Initial position in the middle
    int delayTime = 50;    // Delay time in milliseconds (adjust for desired speed)

    int player1Score = 0;   // Score for player 1
    int player2Score = 0;   // Score for player 2

    // Wait for a button press to start the game
    while (!isLeftButtonPressed() && !isRightButtonPressed())
    {
        // Optional: You can add some LED blinking or other waiting behavior here
    }

    // Reset ball position and scores
    ballPosition = 32;
    player1Score = 0;
    player2Score = 0;

    while (1)
    {
        if (isLeftButtonPressed() && ballPosition <= 5)
        {
            ballDirection = 1 - ballDirection;  // Reverse ball direction
            player1Score++;                     // Increment player 1 score
            _delay_ms(100);    // Delay for button press responsiveness
        }

        if (isRightButtonPressed() && ballPosition >= 60)
        {
            ballDirection = 1 - ballDirection;  // Reverse ball direction
            player2Score++;                     // Increment player 2 score
            _delay_ms(100);    // Delay for button press responsiveness
        }

        moveBall(ballDirection);

        if (ballPosition <= 0 || ballPosition >= 64)
        {
            transmitString("Game Over!\n");
            transmitString("Player 1 Score: ");
            transmitByte(player1Score + '0');   // Convert score to ASCII
            transmitString("\n");
            transmitString("Player 2 Score: ");
            transmitByte(player2Score + '0');   // Convert score to ASCII
            transmitString("\n");
            if (player1Score > player2Score)
            {
                transmitString("Player 1 Won by ");
                transmitByte(player1Score - player2Score + '0');
                transmitString(" points!\n");
            }
            else if (player2Score > player1Score)
            {
                transmitString("Player 2 Won by ");
                transmitByte(player2Score - player1Score + '0');
                transmitString(" points!\n");
            }
            else
            {
                transmitString("It's a Draw!\n");
            }

            enableBuzzer();
            playTone(523.250, 2000);  // Play a tone for 2 seconds

            while (soundPlaying)  // Wait until the sound stops playing
            {
                if (isLeftButtonPressed() || isRightButtonPressed())
                {
                    disableBuzzer();  // Turn off the buzzer if a button is pressed
                    soundPlaying = 0; // Clear the soundPlaying flag
                    break;            // Exit the loop if a button is pressed
                }
            }

            _delay_ms(1000);  // Delay for 1 second before ending the game
            break;            // Ball hit the edge, end the game
        }

        if (ballDirection == 0)
            ballPosition--;  // Move the ball left
        else
            ballPosition++;  // Move the ball right

        // Calculate velocity based on delay time
        float velocity = 1000.0 / delayTime;

        transmitString("Velocity: ");
        transmitByte((int)velocity / 10 + '0');   // Convert tens digit to ASCII
        transmitByte((int)velocity % 10 + '0');   // Convert ones digit to ASCII
        transmitString(" units/s");

        transmitString("  [");
        for (int i = 0; i < 65; i++)
        {
            if (i == ballPosition)
                transmitByte('0');
            else
                transmitByte('-');
        }
        transmitByte(']');  // Print closing bracket without newline
        transmitByte('\r'); // Carriage return to return to the beginning of the line

        _delay_ms(delayTime);
    }

    return 0;
}
